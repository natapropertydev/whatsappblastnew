package com.nataproperty.whatsappblastnew.view.login.model;

public class LoginResponse {
    private String handphone;
    private String waName;

    public LoginResponse(String handphone, String waName) {
        this.handphone = handphone;
        this.waName = waName;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getWaName() {
        return waName;
    }

    public void setWaName(String waName) {
        this.waName = waName;
    }

    @Override
    public String toString() {
        return handphone;
    }
}
