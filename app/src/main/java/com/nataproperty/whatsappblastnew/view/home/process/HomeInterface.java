package com.nataproperty.whatsappblastnew.view.home.process;

import com.nataproperty.whatsappblastnew.base.BaseInterface;
import com.nataproperty.whatsappblastnew.view.home.model.BlastModel;

import java.util.List;

import models.Action;


public interface HomeInterface extends BaseInterface {
    void onSuccessGetListWhatsAppBlast(List<BlastModel> data);
    void onFailGetListWhatsAppBlast(String pMessage);

    void onSuccessGetRespond(String message, Action action);
}
