package com.nataproperty.whatsappblastnew.view.home.ui;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.base.BaseActivity;
import com.nataproperty.whatsappblastnew.config.General;
import com.nataproperty.whatsappblastnew.config.SessionManager;
import com.nataproperty.whatsappblastnew.helper.Utils;
import com.nataproperty.whatsappblastnew.service.ServiceBlast;
import com.nataproperty.whatsappblastnew.service.ServiceChecking;
import com.nataproperty.whatsappblastnew.view.launcher.ui.LauncherActivity;
import com.nataproperty.whatsappblastnew.view.login.ui.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {
    private static final int REQUEST_NOTIFICATION_ACCESS_PERMISSION = 20;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_blast)
    Button btnBlast;
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.btn_stop)
    Button btnStop;
    @BindView(R.id.txt_checking)
    EditText etIntervalChecking;
    @BindView(R.id.txt_blast)
    EditText etIntervalBlast;
    @BindView(R.id.txt_max_count)
    EditText etMaxCount;
    @BindView(R.id.txt_phone)
    EditText etPhoneNumber;
    @BindView(R.id.note_title)
    TextView txtNoteTitle;
    @BindView(R.id.note_description)
    TextView txtNoteDesc;

    private Context mContext;
    private SessionManager mSessionManager;
    private String intervalChecking, intervalBlast, phoneNumber, maxCount;
    private String manufacturer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initToolbar();
        mContext = this;
        mSessionManager = new SessionManager(this);

        phoneNumber = mSessionManager.getStringFromSP(General.PHONE_NUMBER);
        etPhoneNumber.setText(phoneNumber);
        etPhoneNumber.setEnabled(false);

        checkSessionBlast();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
    }

    @Override
    protected void onResume() {

        String enabledListeners = Settings.Secure.getString(mContext.getContentResolver(),
                "enabled_notification_listeners");
        boolean notificationAccess = enabledListeners != null && enabledListeners.contains(mContext.getPackageName());

        super.onResume();
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ServiceBlast.isAccessibilityOn(getApplicationContext(), ServiceBlast.class)) {

                    accessibilityPermission();

                } else if (!notificationAccess){

                    autoReplyPermission();

                } else if(!checkWriteExternalPermission()) {

                    showDialogCekPermission();

                } else {

                    /*intervalChecking = etIntervalChecking.getText().toString().trim();
                    intervalBlast = etIntervalBlast.getText().toString().trim();
                    phoneNumber = etPhoneNumber.getText().toString().trim();
                    maxCount = etMaxCount.getText().toString().trim();*/

                    intervalChecking = "10";
                    intervalBlast = "10";
                    phoneNumber = etPhoneNumber.getText().toString().trim();
                    maxCount = "30";

                    if (isValid(intervalChecking, intervalBlast, phoneNumber, maxCount)) {
                        mSessionManager.setStringToSP(General.INTERVAL_CHECKING, intervalChecking);
                        mSessionManager.setStringToSP(General.INTERVAL_BLAST, intervalBlast);
                        mSessionManager.setStringToSP(General.PHONE_NUMBER, Utils.getNumberOnly(phoneNumber));
                        mSessionManager.setStringToSP(General.MAX_BLAST, maxCount);
                        mSessionManager.setBoolToSP(General.IS_BLAST, true);

                        Intent intent = new Intent(mContext, ServiceChecking.class);
                        mSessionManager.setBoolToSP(General.IS_CHECKING, true);
                        if (Build.VERSION.SDK_INT   >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }

                    /*if (!isMyServiceRunning(ServiceChecking.class)) {
                        Intent intent = new Intent(mContext, ServiceChecking.class);
                        mSessionManager.setBoolToSP(General.IS_CHECKING, true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }
                    }*/

                        setEnableOnButtonClick();
                    }
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMyServiceRunning(ServiceChecking.class)) {
                    Intent intent = new Intent(mContext, ServiceChecking.class);
                    mSessionManager.setBoolToSP(General.IS_CHECKING, false);
                    mSessionManager.setBoolToSP(General.IS_BLAST, false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent);
                    } else {
                        startService(intent);
                    }
                }
                setEnableOnButtonClick();
            }
        });
    }

    private boolean isValid(String checking, String blast, String phoneNumber, String maxCount) {
        if (checking.isEmpty() || Integer.parseInt(checking) <= 0) {
            Utils.showToast(mContext, "Interval Checking must be filled and has to be more than 0");
            return false;
        } else if (blast.isEmpty() || Integer.parseInt(blast) < 5) {
            Utils.showToast(mContext, "Interval Blast must be filled and has to be more than 5");
            return false;
        } else if (phoneNumber.isEmpty()) {
            Utils.showToast(mContext, "Phone Number must be filled and has to be more than 0");
            return false;
        } else if (maxCount.isEmpty() || Integer.parseInt(maxCount) < 30) {
            Utils.showToast(mContext, "Max Count must be filled and has to be less than 30");
            return false;
        } else {
            return true;
        }
    }

    private void checkSessionBlast() {
        if (mSessionManager.getBoolFromSP(General.IS_BLAST)) {
            intervalChecking = mSessionManager.getStringFromSP(General.INTERVAL_CHECKING);
            intervalBlast = mSessionManager.getStringFromSP(General.INTERVAL_BLAST);
            phoneNumber = mSessionManager.getStringFromSP(General.PHONE_NUMBER);
            maxCount = mSessionManager.getStringFromSP(General.MAX_BLAST);

            /*etIntervalChecking.setText(intervalChecking);
            etIntervalBlast.setText(intervalBlast);
            etMaxCount.setText(maxCount);*/
            etPhoneNumber.setText(phoneNumber);

            setEnableOnButtonClick();
        }
    }

    private void setEnableOnButtonClick() {
        if (mSessionManager.getBoolFromSP(General.IS_BLAST)) {
            txtNoteTitle.setText(getString(R.string.title_after_active));
            txtNoteDesc.setText(getString(R.string.description_after_active));
            btnStart.setVisibility(View.GONE);
            /*etIntervalBlast.setEnabled(false);
            etIntervalChecking.setEnabled(false);
            etMaxCount.setEnabled(false);*/
            etPhoneNumber.setEnabled(false);
            btnStop.setVisibility(View.VISIBLE);
        } else {
            txtNoteTitle.setText(getString(R.string.title_before_active));
            txtNoteDesc.setText(getString(R.string.description_before_active));
            btnStart.setVisibility(View.VISIBLE);
            /*etIntervalBlast.setEnabled(true);
            etIntervalChecking.setEnabled(true);
            etMaxCount.setEnabled(true);
            etPhoneNumber.setEnabled(true);*/
            btnStop.setVisibility(View.GONE);
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void accessibilityPermission(){
        manufacturer = Build.MANUFACTURER;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        String addName = getResources().getString(R.string.app_name);
        if (manufacturer.toLowerCase().equals("samsung")) {
            alertDialogBuilder.setMessage(addName + " membutuhkan izin accessibilitas pada halaman\n 'Setting > Accessibility > installed services'.\n\nCari nama " + addName + " untuk mengizinkan aksesibilitas.");
        } else {
            alertDialogBuilder.setMessage(addName + " membutuhkan izin accessibilitas pada halaman\n 'Setting > Accessibility'.\n\nCari nama " + addName + " untuk mengizinkan aksesibilitas.");
        }

        alertDialogBuilder.setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//                  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void autoReplyPermission() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        String addName = getResources().getString(R.string.app_name);
        alertDialogBuilder.setMessage("Mohon untuk mengaktifkan permission 'AutoReply'");

        alertDialogBuilder.setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivity(intent);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_NOTIFICATION_ACCESS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mContext, "Permission Granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                if (mSessionManager.getBoolFromSP(General.IS_BLAST)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Tidak dapat melakukan logout akun, harap untuk mematikan blast terlebih dahulu");
                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                } else {
                    mSessionManager.logoutUser();
                    Intent logoutIntent = new Intent(mContext, LoginActivity.class);
                    logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(logoutIntent);
                    finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}