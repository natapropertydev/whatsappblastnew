package com.nataproperty.whatsappblastnew.view.login.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.view.login.model.LoginResponse;

import java.util.List;

public class NumberSpinnerAdapter extends BaseAdapter {
    private Context context;
    private List<LoginResponse> list;
    private ListNumberHpHolder holder;

    public NumberSpinnerAdapter(Context context, List<LoginResponse> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_number, null);
            holder = new ListNumberHpHolder();
            holder.txtNumberHp = (TextView) convertView.findViewById(R.id.txt_number_hp);
            convertView.setTag(holder);

        } else {
            holder = (ListNumberHpHolder) convertView.getTag();
        }

        holder.txtNumberHp.setText(list.get(position).getHandphone());

        return convertView;
    }

    private class ListNumberHpHolder {
        TextView txtNumberHp;
    }
}
