package com.nataproperty.whatsappblastnew.view.login.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.dialog.MaterialDialogs;
import com.google.android.material.textfield.TextInputLayout;
import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.base.BaseActivity;
import com.nataproperty.whatsappblastnew.config.General;
import com.nataproperty.whatsappblastnew.config.SessionManager;
import com.nataproperty.whatsappblastnew.helper.Utils;
import com.nataproperty.whatsappblastnew.view.launcher.ui.LauncherActivity;
import com.nataproperty.whatsappblastnew.view.login.adapter.NumberSpinnerAdapter;
import com.nataproperty.whatsappblastnew.view.login.model.LoginResponse;
import com.nataproperty.whatsappblastnew.view.login.process.LoginInterface;
import com.nataproperty.whatsappblastnew.view.login.process.LoginPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginInterface {

   /* @BindView(R.id.toolbar)
    Toolbar toolbar;*/
    @BindView(R.id.txtInputLayout_numberHp)
    TextInputLayout txtInputLayout_NumberHP;
    @BindView(R.id.txtInputLayout_password)
    TextInputLayout txtInputLayout_Password;
    @BindView(R.id.et_number_hp)
    EditText etNumberHP;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    private Context context;
    private SessionManager sessionManager;
    private LoginPresenter loginPresenter;
    private NumberSpinnerAdapter adapter;
    private Spinner numberSpinner;
    private ArrayList<LoginResponse> listRespone = new ArrayList<>();

    private String numberHp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
//        initToolbar();
        context = this;

        sessionManager = new SessionManager(this);
        loginPresenter = new LoginPresenter(this);
        loginPresenter.setupView(this);

//        modelResponse();
    }

   /* public void modelResponse() {
        listRespone.add(new LoginResponse("123450", "Andika Wa"));
        listRespone.add(new LoginResponse("678910", "Putri Wa"));
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        checkSessionLogin();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberHp = etNumberHP.getText().toString();
                String password = etPassword.getText().toString();

                if (isValid(numberHp, password)) {

                    // RESPONSE BUATAN
//                    loginPresenter.getRespondBuatan(listRespone);

                    loginPresenter.getRespondLogin(numberHp, password);
                }
            }
        });

    }

    private boolean isValid(String numberHp, String password) {
        if (numberHp == null || numberHp.isEmpty()) {
//            Utils.showToast(context, getResources().getString(R.string.number_hp) + " must be filled");
            txtInputLayout_NumberHP.setError(getResources().getString(R.string.number_hp) + " must be filled" );
            return false;
        } else if (password == null || password.isEmpty()) {
//            Utils.showToast(context, getResources().getString(R.string.password) + " must be filled");
            txtInputLayout_Password.setError(getResources().getString(R.string.password) + " must be filled" );
            return false;
        } else {
            return true;
        }
    }

    /*private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
    }*/

    @Override
    public void showLoader() {

    }

    @Override
    public void dismissLoader() {

    }

    @Override
    public void onNetworkError(String pMessage) {
        Utils.showToast(getApplicationContext(), pMessage);
    }

    @Override
    public void onSuccessLogin(String memberRef, List<LoginResponse> data) {

        if (data.size() == 1) {
            numberHp = data.get(0).getHandphone();
            Intent intent = new Intent(context, LauncherActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

            sessionManager.saveLoginSession(memberRef, numberHp);
            sessionManager.setStringToSP(General.PHONE_NUMBER,numberHp );
        } else {
            dialogForm(memberRef,data);
        }
    }

    @Override
    public void onFailLogin(String pMessage) {
        Utils.showToast(getApplicationContext(), pMessage);
    }

    private void dialogForm(String memberRef, List<LoginResponse> data) {
        adapter = new NumberSpinnerAdapter(this, data);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_choose_phone, null);
        builder.setMessage("Pilih Number Handphone");
        builder.setView(dialogView);

        numberSpinner = (Spinner) dialogView.findViewById(R.id.spinner_number_hp);
        numberSpinner.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        numberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                numberHp = data.get(position).getHandphone();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(context, LauncherActivity.class));

                sessionManager.saveLoginSession(memberRef, numberHp);
                sessionManager.setStringToSP(General.PHONE_NUMBER,numberHp );
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void checkSessionLogin() {
        Intent intent;
        if (sessionManager.isLoggedIn()){
            intent = new Intent(context, LauncherActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }
}