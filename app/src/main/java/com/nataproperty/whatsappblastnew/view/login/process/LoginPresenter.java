package com.nataproperty.whatsappblastnew.view.login.process;

import android.content.Context;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.base.BaseApplication;
import com.nataproperty.whatsappblastnew.base.BasePresenter;
import com.nataproperty.whatsappblastnew.model.RequestResponse;
import com.nataproperty.whatsappblastnew.model.RequestResponseApi;
import com.nataproperty.whatsappblastnew.model.RequestResponseArrayApi;
import com.nataproperty.whatsappblastnew.network.NetworkManager;
import com.nataproperty.whatsappblastnew.view.home.model.BlastModel;
import com.nataproperty.whatsappblastnew.view.login.model.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements BasePresenter<LoginInterface> {

    private Context context;
    private LoginInterface view;
    private NetworkManager networkManager;

    public LoginPresenter(Context context) {
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
        this.networkManager = new NetworkManager();
    }

   /* public void getRespondBuatan(List<LoginResponse> data) {
        if (data != null){
            view.onSuccessLogin("2", data);

        } else {
            view.onFailLogin("Tidak Ada Data");
        }
    }*/

    public void getRespondLogin(String username, String password) {
        view.showLoader();
        Call<RequestResponseArrayApi<LoginResponse>> call = networkManager.getNetworkService().getRespondLogin(username, password);
        call.enqueue(new Callback<RequestResponseArrayApi<LoginResponse>>() {
            @Override
            public void onResponse(Call<RequestResponseArrayApi<LoginResponse>> call, Response<RequestResponseArrayApi<LoginResponse>> response) {
                view.dismissLoader();
                if (response != null && response.isSuccessful()){
                    RequestResponseArrayApi<LoginResponse> body = response.body();
                    if (body.getData() != null && body.getData().size() > 0) {
                        view.onSuccessLogin(body.getMemberRef(), body.getData());
                    } else {
                        view.onFailLogin(context.getResources().getString(R.string.no_data_available));
                    }
                }
            }

            @Override
            public void onFailure(Call<RequestResponseArrayApi<LoginResponse>> call, Throwable t) {
                view.dismissLoader();
                view.onFailLogin(context.getResources().getString(R.string.network_unavailable));
            }
        });
    }

    @Override
    public void setupView(LoginInterface pView) {
        view = pView;
    }

    @Override
    public void clearView() {
        this.view = null;
    }
}
