package com.nataproperty.whatsappblastnew.view.home.model;

public class BlastModel {
    private String receiverHp;
    private String message;
    private String fileUrl;
    private String fileName;

    public BlastModel(String receiverHp, String message, String fileUrl,String fileName) {
        this.receiverHp = receiverHp;
        this.message = message;
        this.fileUrl = fileUrl;
        this.fileName = fileName;
    }

    public String getReceiverHp() {
        return receiverHp;
    }

    public void setReceiverHp(String receiverHp) {
        this.receiverHp = receiverHp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return receiverHp;
    }
}
