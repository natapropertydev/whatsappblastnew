package com.nataproperty.whatsappblastnew.view.home.process;

import android.content.Context;
import android.util.Log;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.base.BaseApplication;
import com.nataproperty.whatsappblastnew.base.BasePresenter;
import com.nataproperty.whatsappblastnew.model.BaseResponse;
import com.nataproperty.whatsappblastnew.network.NetworkManager;
import com.nataproperty.whatsappblastnew.view.home.model.BlastModel;

import java.util.List;

import io.realm.RealmList;
import models.Action;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements BasePresenter<HomeInterface> {

    private Context mContext;
    private HomeInterface mView;
    private NetworkManager mNetwork;
    private List<BlastModel> blastModel;

    public HomePresenter(Context mContext) {
        ((BaseApplication) mContext.getApplicationContext()).getDeps().inject(this);
        this.mContext = mContext;
        this.mNetwork = new NetworkManager();
    }

    /*public void getListWhatsAppBlast(List<BlastModel> data){
        if (data != null){
            mView.onSuccessGetListWhatsAppBlast(data);

        } else {
            mView.onFailGetListWhatsAppBlast("Tidak Ada Data");
        }
    }*/

    public void getListWhatsAppBlast(String phoneNumber){
        mView.showLoader();
        Call<List<BlastModel>> call = mNetwork.getNetworkService().getListWhatsAppBlast(phoneNumber);
        call.enqueue(new Callback<List<BlastModel>>() {
            @Override
            public void onResponse(Call<List<BlastModel>> call, Response<List<BlastModel>> response) {
                mView.dismissLoader();
                if (response != null && response.isSuccessful()){
                    List<BlastModel> data = response.body();
                    if(data != null && data.size() >0){

                        mView.onSuccessGetListWhatsAppBlast(data);

                    } else {
                        mView.onFailGetListWhatsAppBlast(mContext.getResources().getString(R.string.no_data_available));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<BlastModel>> call, Throwable t) {
                mView.dismissLoader();
                mView.onNetworkError(mContext.getResources().getString(R.string.network_unavailable));
            }
        });
    }

    /*public void getRespondMessage(String senderHP, String title, String message, Action action){
            mView.onSuccessGetRespond("Haii, Ini Pesan Dari Autoreply", action);
    }*/

    public void getRespondMessage(String senderHP ,String title, String message, Action action){
        Call<BaseResponse> call = mNetwork.getNetworkService().whatsAppAutoResponder(senderHP, title, message);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    BaseResponse body = response.body();
                    Log.e("Presenter Status", body.getStatus());
                    Log.e("Presenter Message", body.getMessage());
                    if (body.getStatus().equals("200")) {
                        mView.onSuccessGetRespond(body.getMessage(), action);
                    } else {
                        Log.e("TAG_PRESENTER", body.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void setupView(HomeInterface pView) {
        mView = pView;
    }

    @Override
    public void clearView() {
        this.mView = null;
    }
}
