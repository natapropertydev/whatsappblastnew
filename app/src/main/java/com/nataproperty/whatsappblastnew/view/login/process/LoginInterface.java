package com.nataproperty.whatsappblastnew.view.login.process;

import com.nataproperty.whatsappblastnew.base.BaseInterface;
import com.nataproperty.whatsappblastnew.view.login.model.LoginResponse;

import java.util.List;

public interface LoginInterface extends BaseInterface {
    void onSuccessLogin(String memberRef, List<LoginResponse> data);

    void onFailLogin(String pMessage);
}
