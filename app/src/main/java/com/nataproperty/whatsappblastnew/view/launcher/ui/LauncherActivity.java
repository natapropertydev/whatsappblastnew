package com.nataproperty.whatsappblastnew.view.launcher.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.base.BaseActivity;
import com.nataproperty.whatsappblastnew.config.SessionManager;
import com.nataproperty.whatsappblastnew.service.ServiceBlast;
import com.nataproperty.whatsappblastnew.view.home.ui.HomeActivity;

public class LauncherActivity extends BaseActivity {
    private Context mContext;
    private String manufacturer;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        mContext = this;
        manufacturer = Build.MANUFACTURER;
        if (!checkWriteExternalPermission()){
            showDialogCekPermission();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkService();
        checkPermisionAutoReply();
    }

    private void checkService() {
        if (!ServiceBlast.isAccessibilityOn(getApplicationContext(), ServiceBlast.class)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(false);
            String addName = getResources().getString(R.string.app_name);
            if (manufacturer.toLowerCase().equals("samsung")) {
                alertDialogBuilder.setMessage(addName + " membutuhkan izin accessibilitas pada halaman\n 'Setting > Accessibility > installed services'.\n\nCari nama " + addName + " untuk mengizinkan aksesibilitas.");
            } else {
                alertDialogBuilder.setMessage(addName + " membutuhkan izin accessibilitas pada halaman\n 'Setting > Accessibility'.\n\nCari nama " + addName + " untuk mengizinkan aksesibilitas.");
            }

            alertDialogBuilder.setPositiveButton("Lanjut", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//                  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
    }

    private void checkPermisionAutoReply() {

        String enabledListeners = Settings.Secure.getString(mContext.getContentResolver(),
                "enabled_notification_listeners");
        boolean notificationAccess = enabledListeners != null && enabledListeners.contains(mContext.getPackageName());

        if (!notificationAccess) {

            Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent);
        }
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(LauncherActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LauncherActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        } else {
            proceedAfterPermissionDeny();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermissionAllow();
            } else {
                proceedAfterPermissionDeny();
            }
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

}