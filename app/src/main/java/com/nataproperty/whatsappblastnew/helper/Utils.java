package com.nataproperty.whatsappblastnew.helper;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Utils {
    public static String modelToJson(Object pObject) {
        Gson gson = new Gson();
        String jsonIn = gson.toJson(pObject);
        Log.i("utils", "json in " + jsonIn);
        return jsonIn;
    }

    public static String currencyFormat(String pCurrency) {
        if (pCurrency == null) {
            pCurrency = "0";
        }
        double currPrice = Double.valueOf(pCurrency);
        DecimalFormat format = new DecimalFormat("#,###");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static void showSnackbar(View pView, String pMessage) {
        Snackbar.make(pView, pMessage, Snackbar.LENGTH_LONG)
                .show();
    }

    public static void showLog(String pTag, String pLog) {
        if (pLog == null) {
            pLog = "";
        }
        short maxLogSize = 1000;
        if (pLog.length() >= maxLogSize) {
            for (int i = 0; i <= pLog.length() / maxLogSize; ++i) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > pLog.length() ? pLog.length() : end;
                Log.i(pTag, pLog.substring(start, end));
            }
        } else {
            Log.i(pTag, pLog);
        }
    }

    public static void showToast(Context pContext, String pMessage) {
        final Toast toast = Toast.makeText(pContext, pMessage, Toast.LENGTH_LONG);
        toast.show();
    }

    public static String getNumberOnly(String pSrc) {
        return pSrc.replaceAll("\\D+", "");
    }
}
