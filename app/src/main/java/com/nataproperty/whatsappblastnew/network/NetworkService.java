package com.nataproperty.whatsappblastnew.network;


import com.nataproperty.whatsappblastnew.model.BaseResponse;
import com.nataproperty.whatsappblastnew.model.RequestResponseArrayApi;
import com.nataproperty.whatsappblastnew.view.home.model.BlastModel;
import com.nataproperty.whatsappblastnew.view.login.model.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NetworkService {
    @FormUrlEncoded
    @POST("GET_ListWhatsappBlast")
    Call<List<BlastModel>> getListWhatsAppBlast(@Field("handphone") String handphone);

    @FormUrlEncoded
    @POST("POST_whatsappAutoresponder")
    Call<BaseResponse> whatsAppAutoResponder(@Field("senderHP") String senderHP,
                                             @Field("title") String title,
                                             @Field("message") String message);

    @FormUrlEncoded
    @POST("POST_login")
    Call<RequestResponseArrayApi<LoginResponse>> getRespondLogin(@Field("username") String username,
                                                                 @Field("password") String password);
}
