package com.nataproperty.whatsappblastnew.config;

public class General {
    //region Status
    public static final int API_REQUEST_SUCCESS = 200;
    public static final int API_REQUEST_FAIL = 201;
    //endregion

    //region Realm
    public static final int SCHEMA_VERSION = 1;
    public static final String SCHEMA_NAME = "blast.realm";
    //endregion

    //region Shared Preference
    public static final String PREF_NAME = "CoreNata";
    public static final int PRIVATE_MODE = 0;
    //endregion

    public static final String INTERVAL_CHECKING = "intervalChecking";
    public static final String INTERVAL_BLAST = "intervalBlast";
    public static final String PHONE_NUMBER  ="phoneNumber";
    public static final String MAX_BLAST = "maxBlast";
    public static final String IS_CHECKING ="isCheck";
    public static final String IS_BLAST = "isBlast";
    public static final String MEMBERREF = "mamberRef";
    public static final String IS_LOGIN = "isLogin";

}
