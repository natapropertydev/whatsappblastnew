package com.nataproperty.whatsappblastnew.config;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    private static SessionManager instance;

    @SuppressLint("WrongConstant")
    public SessionManager(Context pContext) {
        this.mContext = pContext;
        mPref = this.mContext.getSharedPreferences(General.PREF_NAME, General.PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    public static SessionManager getInstance(Context pContext) {
        if (instance == null) {
            instance = new SessionManager(pContext);
        }
        return instance;
    }

    public String getStringFromSP(String pKey) {
        return mPref.getString(pKey, "");
    }

    public void setStringToSP(String pKey, String pValue) {
        mEditor.putString(pKey, pValue);
        mEditor.commit();
    }

    public void setIntToSP(String pKey, int pValue) {
        mEditor.putInt(pKey, pValue);
        mEditor.commit();
    }

    public int getIntFromSP(String pKey) {
        return mPref.getInt(pKey, 0);
    }

    public void setBoolToSP(String pKey, boolean pValue) {
        mEditor.putBoolean(pKey, pValue);
        mEditor.commit();
    }

    public void saveLoginSession(String mamberRef, String numberHP) {
        mEditor.putString(General.MEMBERREF, mamberRef);
        mEditor.putString(General.PHONE_NUMBER, numberHP);
        mEditor.putBoolean(General.IS_LOGIN, true);
        mEditor.commit();
    }

    public boolean isLoggedIn() {
        return mPref.getBoolean(General.IS_LOGIN, false);
    }

    public void logoutUser() {
        SharedPreferences pref = mContext.getSharedPreferences(General.PREF_NAME, General.PRIVATE_MODE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public boolean getBoolFromSP(String pKey) {
        return mPref.getBoolean(pKey, false);
    }

    /*public void logout() {
        if (mPref != null) {
            mEditor = mPref.edit();
        } else {
            mPref = mContext.getSharedPreferences(General.PREF_NAME, General.PRIVATE_MODE);
        }
        mEditor.clear();
        mEditor.apply();

    }*/
}
