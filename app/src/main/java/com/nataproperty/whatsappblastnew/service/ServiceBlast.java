package com.nataproperty.whatsappblastnew.service;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import java.util.List;

public class ServiceBlast extends AccessibilityService {

    private static final String TAG = ServiceBlast.class.getSimpleName();

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.i(TAG, "***Service Connected***");
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        try {
            if (getRootInActiveWindow() == null) {
                return;
            }
            Log.i(TAG, "***Get Root***");
            AccessibilityNodeInfoCompat rootInActiveWindow = AccessibilityNodeInfoCompat.wrap(getRootInActiveWindow());

            List<AccessibilityNodeInfoCompat> messageNodeList = rootInActiveWindow.findAccessibilityNodeInfosByViewId("com.whatsapp.w4b:id/entry");
            if (messageNodeList == null || messageNodeList.isEmpty()) {
//                return;♦
            }
            Log.i(TAG, "***Get WhatsApp EditText***");

            if (messageNodeList == null || messageNodeList.isEmpty()){
                List<AccessibilityNodeInfoCompat> sendMessageNodeInfoList = rootInActiveWindow.findAccessibilityNodeInfosByViewId("com.whatsapp.w4b:id/send");
                if (sendMessageNodeInfoList == null || sendMessageNodeInfoList.isEmpty()) {
//                    performGlobalAction(GLOBAL_ACTION_BACK);
                    return;
                }
                Log.i(TAG, "***Get Button Send***");

            } else {
                AccessibilityNodeInfoCompat messageField = messageNodeList.get(0);
               /* if (messageField.getText() == null || messageField.getText().length() == 0 || !messageField.getText().toString().endsWith(".")) { // So your service doesn't process any message, but the ones ending your apps suffix
                    return;
                }*/
                if (messageField.getText() == null || messageField.getText().length() == 0) { // So your service doesn't process any message, but the ones ending your apps suffix
                    return;
                }
                Log.i(TAG, "***Suffix True***");
            }

            // WhatsApp send button id
            List<AccessibilityNodeInfoCompat> sendMessageNodeInfoList = rootInActiveWindow.findAccessibilityNodeInfosByViewId("com.whatsapp.w4b:id/send");
            if (sendMessageNodeInfoList == null || sendMessageNodeInfoList.isEmpty()) {
                return;
            }
            Log.i(TAG, "***Get Button Send***");

            AccessibilityNodeInfoCompat sendMessageButton = sendMessageNodeInfoList.get(0);
            if (!sendMessageButton.isVisibleToUser()) {
                return;
            }

            // Now fire a click on the send button
            sendMessageButton.performAction(AccessibilityNodeInfo.ACTION_CLICK);
            Log.i(TAG, "***SEND***");

            // Now go back to your app by clicking on the Android back button twice:
            // First one to leave the conversation screen
            // Second one to leave whatsApp
            Thread.sleep(500); // hack for certain devices in which the immediate back click is too fast to handle
            performGlobalAction(GLOBAL_ACTION_BACK);
            Thread.sleep(500);  // same hack as above
            performGlobalAction(GLOBAL_ACTION_BACK);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onInterrupt() {
        Log.i(TAG, "***Service Interrupted***");
    }

    public static boolean isAccessibilityOn(Context context, Class<? extends AccessibilityService> clazz) {
        int accessibilityEnabled = 0;
        final String service = context.getPackageName() + "/" + clazz.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(context.getApplicationContext().getContentResolver(), Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException ignored) {
        }

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                colonSplitter.setString(settingValue);
                while (colonSplitter.hasNext()) {
                    String accessibilityService = colonSplitter.next();

                    if (accessibilityService.equalsIgnoreCase(service)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
