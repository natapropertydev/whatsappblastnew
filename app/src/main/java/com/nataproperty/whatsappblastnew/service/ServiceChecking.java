package com.nataproperty.whatsappblastnew.service;

import android.Manifest;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.SpannedString;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.nataproperty.whatsappblastnew.R;
import com.nataproperty.whatsappblastnew.config.General;
import com.nataproperty.whatsappblastnew.config.SessionManager;
import com.nataproperty.whatsappblastnew.helper.Utils;
import com.nataproperty.whatsappblastnew.view.home.model.BlastModel;
import com.nataproperty.whatsappblastnew.view.home.process.HomeInterface;
import com.nataproperty.whatsappblastnew.view.home.process.HomePresenter;
import com.nataproperty.whatsappblastnew.view.home.ui.HomeActivity;
import com.robj.notificationhelperlibrary.utils.NotificationUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import models.Action;


public class ServiceChecking extends NotificationListenerService implements HomeInterface {

    private static final String CHANNEL_ID = "ServiceChecking";
    private static final String TAG_v1 = "NLService";
    private static final String TAG = ServiceChecking.class.getSimpleName();
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    private Timer timer;
    private TimerTask timerTask;
    private String second;
    private SessionManager mSessionManager;
    private HomePresenter presenter;
    private Notification notification;
    private List<BlastModel> blastModel = new ArrayList<>();
    private Context mContext;
    private String fileName, urlFile;
    private DownloadManager downloadManager;

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        showNotificationForeground();
//        modelResponse();
    }

   /* @Override
    public IBinder onBind(Intent intent) {
        return null;
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "***onServiceDestroyed***");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        presenter = new HomePresenter(this);
        presenter.setupView(this);
        mContext = this;
        mSessionManager = new SessionManager(getApplicationContext());
        if (mSessionManager.getBoolFromSP(General.IS_CHECKING)) {
            startTimer();
        }
        return START_STICKY;
    }

    /*public void modelResponse() {
        blastModel.add(new BlastModel("6281324144563", "Hallo, Ini Pesan Dari WaBlast", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqLSMgW13ZByPHrVovkfMB7ynQGu9DXCbAkw&usqp=CAU", "TesGambar1"));
//        blastModel.add(new BlastModel("62895617", "Hallo, Ini Pesan Dari WaBlast", "",""));
        blastModel.add(new BlastModel("62895617061954", "Hallo, Ini Pesan Dari WaBlast", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqLSMgW13ZByPHrVovkfMB7ynQGu9DXCbAkw&usqp=CAU", "TesGambar2"));
//        blastModel.add(new BlastModel("2", "6285920501294", "Hallo, Ini Pesan Dari WaBlasta", "/storage/emulated/0/Download/nataproperty/Millennium City-_MG_6620 compressed.jpg"));
    }*/

    final Handler handler = new Handler(Looper.getMainLooper());

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        second = mSessionManager.getStringFromSP(General.INTERVAL_CHECKING);
        timer.schedule(timerTask, 1000, toMinute(Integer.parseInt(second)));
    }

    private int toMinute(int s) {
        int second = 1 * 1000;
        return s * second;
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        String hp = mSessionManager.getStringFromSP(General.PHONE_NUMBER);
                        String count = mSessionManager.getStringFromSP(General.MAX_BLAST);
                        if (mSessionManager.getBoolFromSP(General.IS_CHECKING)) {
                            //RESPON BUATAN
//                            presenter.getListWhatsAppBlast(blastModel);

                            //RESPONSE DARI SERVICE
                            presenter.getListWhatsAppBlast(hp);
                        }
//                        presenter.getListWhatsAppBlast(hp);
                    }
                });
            }
        };
    }

    //AutoReply
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        Log.d(TAG_v1, "***Notification Removed***");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.d(TAG_v1, "***Notification Received***");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            NLService.this.cancelNotification(sbn.getKey());
            String packageName = sbn.getPackageName();
            Log.d(TAG, packageName);
                if (!packageName.toLowerCase().equals("com.whatsapp.w4b")) {
                return;
            }

            Notification notification = sbn.getNotification();
            Bundle extras = notification.extras;

            Object isGroupConversation = extras.get(NotificationCompat.EXTRA_IS_GROUP_CONVERSATION);
            String groupConversation = extras.getString(NotificationCompat.EXTRA_CONVERSATION_TITLE);

            boolean isGroup = false;
            if (isGroupConversation != null) {
                isGroup = (((boolean) isGroupConversation) && (groupConversation != null));
                Log.d(TAG, "isGroup = " + isGroup);
            }

            if (!isGroup) {
                Action action = NotificationUtils.getQuickReplyAction(notification, getPackageName());
                if (action != null) {
                    try {

                        String title = extras.getString(NotificationCompat.EXTRA_TITLE);
                        CharSequence charSeq = extras.getCharSequence(NotificationCompat.EXTRA_TEXT);
                        SpannedString spanned = new SpannedString(charSeq);
                        String message = spanned.toString();
                        String hp = mSessionManager.getStringFromSP(General.PHONE_NUMBER);

                        if (mSessionManager.getBoolFromSP(General.IS_BLAST)){

                            /*if (message.contains("hello")) {
                                presenter.getRespondMessage(hp, title, message, action);
                            }*/

                            presenter.getRespondMessage(hp, title, message, action);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            super.onNotificationPosted(sbn);
        }
    }

    @Override
    public void onSuccessGetListWhatsAppBlast(List<BlastModel> data) {
        if (data.size() > 0) {
            stopTimerTask();

            if (checkWriteExternalPermission()) {
                for (int i = 0; i < data.size(); i++) {
                    fileName = data.get(i).getFileName();
                    urlFile = data.get(i).getFileUrl();

                    if (!urlFile.isEmpty()) {

                        String pathImage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/natapropertyWaAuto/" + data.get(i).getFileName();
                        File checkFile = new File(pathImage);

                        if (!checkFile.exists()){
                            DownloadImage(fileName, urlFile);
                        }
                    }
                }

                doBlast(data);

            } else {
                Toast.makeText(getBaseContext(), "Permission Belum diAktifkan", Toast.LENGTH_LONG).show();
            }

        } else {
            Log.i(TAG, "***IsChecking True***");
            mSessionManager.setBoolToSP(General.IS_CHECKING, true);
        }
    }

    @Override
    public void onFailGetListWhatsAppBlast(String pMessage) {
        Utils.showToast(getApplicationContext(), pMessage);
    }

    public void DownloadImage(String fileName, String url) {
        Picasso.with(getApplicationContext()).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    File mydir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/natapropertyWaAuto/");
                    if (!mydir.exists()) {
                        mydir.mkdirs();
                    }

                    String fileUri = mydir.getAbsolutePath() + File.separator + fileName + ".jpg";
                    FileOutputStream outputStream = new FileOutputStream(fileUri);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    outputStream.flush();
                    outputStream.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(), "Image Downloaded", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }

        });
    }

    public void downloadFile(String fileName, String url) {

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        File mydir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/natapropertyWaAuto/");
        if (!mydir.exists()) {
            mydir.mkdirs();
        }

        DownloadManager downloadManager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);

//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
//                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS + "/natapropertyWaAuto/", fileName + ".pdf");
//        long reference = downloadManager.enqueue(request);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Demo")
                .setDescription("Something useful. No, really.")
                .setDestinationInExternalPublicDir("/AnhsirkDasarpFiles", "fileName.jpg");

        downloadManager.enqueue(request);
    }

    @Override
    public void onSuccessGetRespond(String message, Action action) {
        try {
            action.sendReply(this, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void dismissLoader() {

    }

    @Override
    public void onNetworkError(String pMessage) {
        Utils.showToast(getApplicationContext(), pMessage);
    }

    public void stopTimerTask() {
        /*if (timer != null) {
            timer.cancel();
            timer = null;
        }*/
        Log.i(TAG, "***IsChecking False***");
        mSessionManager.setBoolToSP(General.IS_CHECKING, false);
    }

    private void doBlast(List<BlastModel> data) {
        if (!ServiceBlast.isAccessibilityOn(getApplicationContext(), ServiceBlast.class)) {
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            sendBlast(0, data, data.size());
        }
    }

    private void sendBlast(int i, List<BlastModel> data, int total) {
        // Interval Blast Set
        String second = mSessionManager.getStringFromSP(General.INTERVAL_BLAST);
        long mills = (long) (Integer.parseInt(second) * 1000);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (i < total) {
                        String number = data.get(i).getReceiverHp();
                        String pMessage = data.get(i).getMessage();
//                        String text = pMessage + ".";
                        String text = pMessage;
                        String pathImage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/natapropertyWaAuto/" + data.get(i).getFileName() + ".jpg";
                        File checkFile = new File(pathImage);
                        Uri fileUri = Uri.parse(checkFile.getAbsolutePath());

                        if (!data.get(i).getFileUrl().isEmpty()) {

                            if (checkFile.exists()) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.putExtra("jid", number + "@s.whatsapp.net");
                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_STREAM, fileUri);
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_SEND);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setPackage("com.whatsapp.w4b");
                                startActivity(intent);
                            } else {
                                Toast.makeText(getBaseContext(), "File tidak ada", Toast.LENGTH_LONG).show();
                            }

                        } else {
                            String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode(text, "UTF-8");
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setPackage("com.whatsapp.w4b");
                            intent.setData(Uri.parse(url));
                            startActivity(intent);
                        }

                        sendBlast(i + 1, data, total);

                    } else {
                        mSessionManager.setBoolToSP(General.IS_CHECKING, true);
                        Utils.showToast(getApplicationContext(), "Finished");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, mills);
    }

    private void showNotificationForeground() {
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, TAG, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableVibration(true);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this);
        }
        notificationBuilder
                .setContentTitle(getApplicationContext().getString(R.string.app_name))
                .setContentText("Service Running")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLargeIcon(icon)
                .setColor(Color.RED)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, TAG,
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);

            notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID).build();
        } else {
            notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).build();
        }
        startForeground(1, notification);
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}
