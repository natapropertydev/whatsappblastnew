package com.nataproperty.whatsappblastnew.dagger;



import com.nataproperty.whatsappblastnew.view.home.process.HomePresenter;
import com.nataproperty.whatsappblastnew.view.login.process.LoginPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        SessionManagerModule.class
})
public interface Deps {
    void inject(HomePresenter presenter);

    void inject(LoginPresenter presenter);

}
