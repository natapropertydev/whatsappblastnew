package com.nataproperty.whatsappblastnew.dagger;

import android.content.Context;


import com.nataproperty.whatsappblastnew.config.SessionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SessionManagerModule {
    @Provides
    @Singleton
    SessionManager providesSessionManager(Context pContext){
        return new SessionManager(pContext);
    }
}
