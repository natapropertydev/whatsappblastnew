package com.nataproperty.whatsappblastnew.base;

public interface BasePresenter<T extends BaseInterface> {
    void setupView(T pView);

    void clearView();
}
