package com.nataproperty.whatsappblastnew.base;

public interface BaseInterface {
    void showLoader();

    void dismissLoader();

    void onNetworkError(String pMessage);
}
