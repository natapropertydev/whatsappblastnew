package com.nataproperty.whatsappblastnew.base;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.nataproperty.whatsappblastnew.config.General;
import com.nataproperty.whatsappblastnew.dagger.ApplicationModule;

import com.nataproperty.whatsappblastnew.dagger.DaggerDeps;
import com.nataproperty.whatsappblastnew.dagger.Deps;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {
    private Deps mDeps;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDeps = initDagger(this);
        MultiDex.install(this);
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(General.SCHEMA_VERSION)
                .deleteRealmIfMigrationNeeded()
                .name(General.SCHEMA_NAME)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public Deps getDeps() {
        return mDeps;
    }

    protected Deps initDagger(BaseApplication application) {
        return DaggerDeps.builder()
                .applicationModule(new ApplicationModule(application))
                .build();
    }
}
